from functools import wraps


def get_str_values(obj, headers):
    values = list()
    for header in headers:
        value = obj.__dict__[header]

        if value is None:
            values.append("NULL")
        else:
            values.append(repr(value))

    return values


def entity2querystr(obj, entity):
    headers = entity.get_headers(without_id=True)

    fields = get_str_values(obj, headers)
    result = "({})".format(", ".join(fields))

    return result


def query2entity(query, headers, entity):
    instances = list()
    for data in query:
        field_dict = dict(zip(headers, data))
        instance = entity(**field_dict)
        instances.append(instance)

    return instances


def trans_wrapper(f):
    @wraps(f)
    def new_f(self, *args, **kwargs):
        self.cursor = self.conn.cursor()
        result = f(self, *args, **kwargs)
        self.cursor.close()
        self.conn.commit()

        return result

    return new_f
