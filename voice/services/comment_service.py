from voice.services.service_utils import (
    get_str_values,
    query2entity,
    entity2querystr,
    trans_wrapper,
)
from voice.models.comment import Comment


class CommentService:
    def __init__(self, conn):
        self.conn = conn

        self.verify_table()

    @trans_wrapper
    def add(self, comment):
        headers = Comment.get_headers(without_id=True)

        query = "insert into comments ({}) values {}".format(", ".join(headers), entity2querystr(comment, Comment))
        print(query)
        self.cursor.execute(query)

        return self.conn.insert_id()

    @trans_wrapper
    def select_by_post(self, post_id):
        headers = Comment.get_headers()
        query = "select {} from comments where post_id = {}".format(
            ", ".join(headers),
            post_id,
        )
        print(query)
        self.cursor.execute(query)

        comments = query2entity(self.cursor, headers, Comment)

        return comments

    @trans_wrapper
    def select(self, ids):
        headers = Comment.get_headers()
        query = "select {} from comments where id in ({})".format(", ".join(headers), ", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

        comments = query2entity(self.cursor, headers, Comment)

        return comments

    @trans_wrapper
    def remove(self, ids):
        query = "delete from comments where id in ({})".format(", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def update(self, comment):
        headers = Comment.get_headers(without_id=True)
        values = get_str_values(comment, headers)
        query = "update comments set {} where id = {}".format(
            ", ".join(["{}={}".format(header, value) for header, value in zip(headers, values)]),
            comment.id,
        )
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def drop_tables(self):
        self.cursor.execute("drop table if exists comments")

    @trans_wrapper
    def verify_table(self):
        table_description = ("create table if not exists comments ("
                             "   id integer primary key auto_increment,"
                             "   content varchar(200) not null,"
                             "   creator_id integer,"
                             "   post_id integer,"
                             "   constraint comment_creator_id_fk"
                             "   foreign key (creator_id)"
                             "   references fakes(id)"
                             "       on delete cascade"
                             "       on update cascade,"
                             "   constraint comment_post_id_fk"
                             "   foreign key (post_id)"
                             "   references posts(id)"
                             "       on delete cascade"
                             "       on update cascade"
                             ")")

        self.cursor.execute(table_description)
