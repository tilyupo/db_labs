from voice.services.service_utils import (
    get_str_values,
    query2entity,
    entity2querystr,
    trans_wrapper,
)
from voice.models.post import Post


class PostService:
    def __init__(self, conn):
        self.conn = conn

        self.verify_table()

    @trans_wrapper
    def add(self, post):
        headers = Post.get_headers(without_id=True)

        query = "insert into posts ({}) values {}".format(", ".join(headers), entity2querystr(post, Post))
        print(query)
        self.cursor.execute(query)

        return self.conn.insert_id()

    @trans_wrapper
    def select_by_blog(self, blog_id):
        headers = Post.get_headers()
        query = "select {} from posts where blog_id = {}".format(
            ", ".join(headers),
            blog_id,
        )
        print(query)
        self.cursor.execute(query)

        posts = query2entity(self.cursor, headers, Post)

        return posts

    @trans_wrapper
    def select(self, ids):
        headers = Post.get_headers()
        query = "select {} from posts where id in ({})".format(", ".join(headers), ", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

        posts = query2entity(self.cursor, headers, Post)

        return posts

    @trans_wrapper
    def remove(self, ids):
        query = "delete from posts where id in ({})".format(", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def update(self, post):
        headers = Post.get_headers(without_id=True)
        values = get_str_values(post, headers)
        query = "update posts set {} where id = {}".format(
            ", ".join(["{}={}".format(header, value) for header, value in zip(headers, values)]),
            post.id,
        )
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def drop_tables(self):
        self.cursor.execute("drop table if exists posts")

    @trans_wrapper
    def verify_table(self):
        table_description = ("create table if not exists posts ("
                             "   id integer primary key auto_increment,"
                             "   title varchar(200) not null,"
                             "   content varchar(2000),"
                             "   category_id integer,"
                             "   blog_id integer,"
                             "   constraint category_id_fk"
                             "   foreign key (category_id)"
                             "   references categorys(id)"
                             "       on delete cascade"
                             "       on update cascade,"
                             "   constraint blog_id_fk"
                             "   foreign key (blog_id)"
                             "   references blogs(id)"
                             "       on delete cascade"
                             "       on update cascade"
                             ")")

        self.cursor.execute(table_description)
