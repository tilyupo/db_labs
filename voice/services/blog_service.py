from voice.services.service_utils import (
    get_str_values,
    query2entity,
    entity2querystr,
    trans_wrapper,
)
from voice.models.blog import Blog


class BlogService:
    def __init__(self, conn):
        self.conn = conn

        self.verify_table()

    @trans_wrapper
    def add(self, blog):
        headers = Blog.get_headers(without_id=True)

        query = "insert into blogs ({}) values {}".format(", ".join(headers), entity2querystr(blog, Blog))
        print(query)
        self.cursor.execute(query)

        return self.conn.insert_id()

    @trans_wrapper
    def select_by_owner(self, creator_id):
        headers = Blog.get_headers()
        query = "select {} from blogs where creator_id = {}".format(
            ", ".join(headers),
            creator_id,
        )
        print(query)
        self.cursor.execute(query)

        blogs = query2entity(self.cursor, headers, Blog)

        return blogs

    @trans_wrapper
    def get_all(self):
        headers = Blog.get_headers()
        query = "select {} from blogs".format(", ".join(headers))
        print(query)
        self.cursor.execute(query)

        blogs = query2entity(self.cursor, headers, Blog)

        return blogs

    @trans_wrapper
    def select(self, ids):
        headers = Blog.get_headers()
        query = "select {} from blogs where id in ({})".format(", ".join(headers), ", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

        blogs = query2entity(self.cursor, headers, Blog)

        return blogs

    @trans_wrapper
    def remove(self, ids):
        query = "delete from blogs where id in ({})".format(", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def update(self, blog):
        headers = Blog.get_headers(without_id=True)
        values = get_str_values(blog, headers)
        query = "update blogs set {} where id = {}".format(
            ", ".join(["{}={}".format(header, value) for header, value in zip(headers, values)]),
            blog.id,
        )
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def drop_tables(self):
        self.cursor.execute("drop table if exists blogs")

    @trans_wrapper
    def verify_table(self):
        table_description = ("create table if not exists blogs ("
                             "   id integer primary key auto_increment,"
                             "   title varchar(200) not null,"
                             "   creator_id integer,"
                             "   constraint blog_creator_id_fk"
                             "   foreign key (creator_id)"
                             "   references fakes(id)"
                             "       on delete cascade"
                             "       on update cascade"
                             ")")

        self.cursor.execute(table_description)
