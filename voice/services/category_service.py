from voice.services.service_utils import (
    get_str_values,
    query2entity,
    entity2querystr,
    trans_wrapper,
)
from voice.models.category import Category


class CategoryService:
    def __init__(self, conn):
        self.conn = conn

        self.verify_table()

    @trans_wrapper
    def add(self, category):
        headers = Category.get_headers(without_id=True)

        query = "insert into categorys ({}) values {}".format(", ".join(headers), entity2querystr(category, Category))
        print(query)
        self.cursor.execute(query)

        return self.conn.insert_id()

    @trans_wrapper
    def select_all(self):
        headers = Category.get_headers()
        query = "select {} from categorys".format(", ".join(headers), )
        print(query)
        self.cursor.execute(query)

        categorys = query2entity(self.cursor, headers, Category)

        return categorys

    @trans_wrapper
    def select(self, ids):
        headers = Category.get_headers()
        query = "select {} from categorys where id in ({})".format(", ".join(headers), ", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

        categorys = query2entity(self.cursor, headers, Category)

        return categorys

    @trans_wrapper
    def remove(self, ids):
        query = "delete from categorys where id in ({})".format(", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def update(self, category):
        headers = Category.get_headers(without_id=True)
        values = get_str_values(category, headers)
        query = "update categorys set {} where id = {}".format(
            ", ".join(["{}={}".format(header, value) for header, value in zip(headers, values)]),
            category.id,
        )
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def drop_tables(self):
        self.cursor.execute("drop table if exists categorys")

    @trans_wrapper
    def verify_table(self):
        table_description = ("create table if not exists categorys ("
                             "   id integer primary key auto_increment,"
                             "   name varchar(20) not null"
                             ")")

        self.cursor.execute(table_description)
