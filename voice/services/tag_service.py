from voice.services.service_utils import (
    get_str_values,
    query2entity,
    entity2querystr,
    trans_wrapper,
)
from voice.models.tag import Tag


class TagService:
    def __init__(self, conn):
        self.conn = conn

        self.verify_table()

    @trans_wrapper
    def add(self, tag):
        headers = Tag.get_headers(without_id=True)

        query = "insert into tags ({}) values {}".format(", ".join(headers), entity2querystr(tag, Tag))
        print(query)
        self.cursor.execute(query)

        return self.conn.insert_id()

    @trans_wrapper
    def select(self, ids):
        headers = Tag.get_headers()
        query = "select {} from tags where id in ({})".format(", ".join(headers), ", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

        tags = query2entity(self.cursor, headers, Tag)

        return tags

    @trans_wrapper
    def remove(self, ids):
        query = "delete from tags where id in ({})".format(", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def update(self, tag):
        headers = Tag.get_headers(without_id=True)
        values = get_str_values(tag, headers)
        query = "update tags set {} where id = {}".format(
            ", ".join(["{}={}".format(header, value) for header, value in zip(headers, values)]),
            tag.id,
        )
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def drop_tables(self):
        self.cursor.execute("drop table if exists tags")

    @trans_wrapper
    def verify_table(self):
        table_description = ("create table if not exists tags ("
                             "   id integer primary key auto_increment,"
                             "   name varchar(20) not null"
                             ")")

        self.cursor.execute(table_description)
