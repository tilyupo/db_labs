from voice.services.service_utils import (
    get_str_values,
    query2entity,
    entity2querystr,
    trans_wrapper,
)
from voice.models.user import User


class UserService:
    def __init__(self, conn):
        self.conn = conn

        self.verify_table()

    @trans_wrapper
    def add(self, user):
        headers = User.get_headers(without_id=True)

        query = "insert into users ({}) values {}".format(", ".join(headers), entity2querystr(user, User))
        print(query)
        self.cursor.execute(query)

        return self.conn.insert_id()

    @trans_wrapper
    def select(self, ids):
        headers = User.get_headers()
        query = "select {} from users where id in ({})".format(", ".join(headers), ", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

        users = query2entity(self.cursor, headers, User)

        return users

    @trans_wrapper
    def remove(self, ids):
        query = "delete from users where id in ({})".format(", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def update(self, user):
        headers = User.get_headers(without_id=True)
        values = get_str_values(user, headers)
        query = "update users set {} where id = {}".format(
            ", ".join(["{}={}".format(header, value) for header, value in zip(headers, values)]),
            user.id,
        )
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def select_by_logins(self, logins):
        headers = User.get_headers()
        query = "select {} from users where login in ({})".format(", ".join(headers), ", ".join(
            repr(login) for login in logins))
        print(query)
        self.cursor.execute(query)

        users = query2entity(self.cursor, headers, User)

        return users

    @trans_wrapper
    def drop_tables(self):
        self.cursor.execute("drop table if exists users")

    @trans_wrapper
    def verify_table(self):
        table_description = ("create table if not exists users ("
                             "   id integer primary key auto_increment,"
                             "   login varchar(20) not null unique,"
                             "   password_hash varchar(20)"
                             ")")

        self.cursor.execute(table_description)
