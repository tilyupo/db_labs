from voice.services.service_utils import (
    get_str_values,
    query2entity,
    entity2querystr,
    trans_wrapper,
)
from voice.models.fake import Fake


class FakeService:
    def __init__(self, conn):
        self.conn = conn

        self.verify_table()

    @trans_wrapper
    def add(self, fake):
        headers = Fake.get_headers(without_id=True)

        query = "insert into fakes ({}) values {}".format(", ".join(headers), entity2querystr(fake, Fake))
        print(query)
        self.cursor.execute(query)

        return self.conn.insert_id()

    @trans_wrapper
    def select(self, ids):
        headers = Fake.get_headers()
        query = "select {} from fakes where id in ({})".format(", ".join(headers), ", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

        fakes = query2entity(self.cursor, headers, Fake)

        return fakes

    @trans_wrapper
    def select_by_user(self, user_id):
        headers = Fake.get_headers()
        query = "select {} from fakes where user_id = {}".format(
            ", ".join(headers),
            user_id,
        )
        print(query)
        self.cursor.execute(query)

        fakes = query2entity(self.cursor, headers, Fake)

        return fakes

    @trans_wrapper
    def select_by_users(self, user_ids):
        headers = Fake.get_headers()
        query = "select {} from fakes where user_id in ({})".format(
            ", ".join(headers),
            ", ".join(str(x) for x in user_ids),
        )
        print(query)
        self.cursor.execute(query)

        fakes = query2entity(self.cursor, headers, Fake)

        return fakes

    @trans_wrapper
    def select_by_names(self, user_id, names):
        headers = Fake.get_headers()
        query = "select {} from fakes where user_id = {} and nickname in ({})".format(
            ", ".join(headers), user_id, ", ".join([repr(name) for name in names]))
        print(query)
        self.cursor.execute(query)

        fakes = query2entity(self.cursor, headers, Fake)

        return fakes

    @trans_wrapper
    def remove(self, ids):
        query = "delete from fakes where id in ({})".format(", ".join(str(x) for x in ids))
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def update(self, fake):
        headers = Fake.get_headers(without_id=True)
        values = get_str_values(fake, headers)
        query = "update fakes set {} where id = {}".format(
            ", ".join(["{}={}".format(header, value) for header, value in zip(headers, values)]),
            fake.id,
        )
        print(query)
        self.cursor.execute(query)

    @trans_wrapper
    def drop_tables(self):
        self.cursor.execute("drop table if exists fakes")

    @trans_wrapper
    def verify_table(self):
        table_description = ("create table if not exists fakes ("
                             "   id integer primary key auto_increment,"
                             "   nickname varchar(20) not null,"
                             "   user_id integer,"
                             "   constraint fake_user_id_fk"
                             "   foreign key (user_id)"
                             "   references users(id)"
                             "       on delete cascade"
                             "       on update cascade"
                             ")")

        self.cursor.execute(table_description)
