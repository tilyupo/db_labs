from sqlalchemy.schema import DropTable
from sqlalchemy.ext.compiler import compiles


def get_ids(collection):
    return [item.id for item in collection]


def get_names(collection):
    return [item.name for item in collection]


@compiles(DropTable, "postgresql")
def _compile_drop_table(element, compiler, **kwargs):
    """
        Need for droping tables with CASCADE options.
    """
    return compiler.visit_drop_table(element) + " CASCADE"
