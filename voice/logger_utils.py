"""
    Provides logger handling functions.
"""

import logging


def get_logger():
    logger = logging.getLogger("voice-build_18.02")

    return logger


def disable_logger():
    logger = get_logger()
    logger.disabled = True


def enable_logger():
    logger = get_logger()
    logger.disabled = False
