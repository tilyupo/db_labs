from voice.services import (
    PostService,
    BlogService,
    UserService,
    FakeService,
    CommentService,
    TagService,
    CategoryService,
)

# from mysql import connector
# import mysql.connector
import pymysql
import re


class Voice:
    def __init__(self, db_string):
        pattern = r"^(\w+)://(\w+):(\w+)@(\w+)/(\w+)$"
        reply = re.findall(pattern, db_string)
        if len(reply) == 0:
            raise Exception("Invalid db_string")
        reply = reply[0]

        db_type, user, password, host, db_name = reply
        if db_type != "mysql":
            raise Exception("Only mysql database is available.")

        self.conn = pymysql.connect(host=host, user=user, db=db_name, passwd=password)

        self.user_service = UserService(self.conn)
        self.fake_service = FakeService(self.conn)
        self.category_service = CategoryService(self.conn)
        self.tag_service = TagService(self.conn)
        self.blog_service = BlogService(self.conn)
        self.post_service = PostService(self.conn)
        self.comment_service = CommentService(self.conn)

    def drop_all_tables(self):
        self.comment_service.drop_tables()
        self.post_service.drop_tables()
        self.blog_service.drop_tables()
        self.tag_service.drop_tables()
        self.category_service.drop_tables()
        self.fake_service.drop_tables()
        self.user_service.drop_tables()

    def __del__(self):
        self.conn.close()
