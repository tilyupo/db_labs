import unittest

from voice.tests.test_utils import get_half_filled_voice
from voice.exceptions import (
    ObjectDontExistsError
)


class RemovingTests(unittest.TestCase):
    def setUp(self):
        self.voice = get_half_filled_voice()
        self.id = 1

    def test_removing_user(self):
        obj = self.voice.get_object("user", self.id)
        self.voice.remove("user", self.id)
        with self.assertRaises(ObjectDontExistsError):
            obj = self.voice.get_object("user", self.id)

    def test_removing_task(self):
        obj = self.voice.get_object("task", self.id)
        self.voice.remove("task", self.id)
        with self.assertRaises(ObjectDontExistsError):
            obj = self.voice.get_object("task", self.id)

    def test_removing_supertask(self):
        obj = self.voice.get_object("supertask", self.id)
        self.voice.remove("supertask", self.id)
        with self.assertRaises(ObjectDontExistsError):
            obj = self.voice.get_object("supertask", self.id)

    def test_removing_tag(self):
        obj = self.voice.get_object("tag", self.id)
        self.voice.remove("tag", self.id)
        with self.assertRaises(ObjectDontExistsError):
            obj = self.voice.get_object("tag", self.id)

    def test_removing_comment(self):
        obj = self.voice.get_object("comment", self.id)
        self.voice.remove("comment", self.id)
        with self.assertRaises(ObjectDontExistsError):
            obj = self.voice.get_object("comment", self.id)

    def test_removing_notification(self):
        obj = self.voice.get_object("notification", self.id)
        self.voice.remove("notification", self.id)
        with self.assertRaises(ObjectDontExistsError):
            obj = self.voice.get_object("notification", self.id)

    def test_removing_not_existed(self):
        obj = self.voice.get_object("user", self.id)
        self.voice.remove("user", self.id)
        with self.assertRaises(ObjectDontExistsError):
            obj = self.voice.remove("user", self.id)

    def voicerDown(self):
        del self.voice
