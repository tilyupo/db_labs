from datetime import datetime, timedelta
from ..voice import Voice
from ..models import (User, Task, SuperTask, Tag, Comment,
                      Notification, NotificationKind, SuperTaskKind)


def check_dict_matching(obj, init_dict):
    for key, value in init_dict.items():
        if not hasattr(obj, key) or getattr(obj, key) != value:
            raise ValueError(
                "Obj {} doesn't match init_dict {}".format(
                    obj,
                    init_dict,
                )
            )


def get_empty_voice():
    voice = Voice("sqlite://")
    voice.clear()

    return voice


def get_half_filled_voice():
    voice = get_empty_voice()

    anton_id = voice.add_user(
        name="Anton",
    )

    alex_id = voice.add_user(
        name="Alex",
    )

    voice.add_task(
        description="First level task",
        deadline=datetime.strptime(
            "18/06/2017 18:20",
            get_date_format()
        ),
    )

    voice.add_task(
        description="Second level task",
        deadline=datetime.strptime(
            "19/06/2017 18:20",
            get_date_format()
        ),
    )

    voice.add_task(
        description="Third level task",
        deadline=datetime.strptime(
            "20/06/2017 19:20",
            get_date_format()
        ),
    )

    voice.add_supertask(
        creator_id=anton_id,
        kind=SuperTaskKind.INTERVAL_KIND,
        title="Meat",
        description="Eat Meat",
        creation_time=datetime.strptime("11/11/2008 16:10",
                                        get_date_format()),
        first_creation_time=datetime.strptime("12/11/2008 16:10",
                                              get_date_format()),
        re_creation_period=timedelta(6000),
        time_to_execution=timedelta(600),
    )

    voice.add_tag(
        name="book",
        creation_time=datetime.strptime("11/11/2008 16:10",
                                        get_date_format())
    )

    voice.add_comment(
        text="Some text",
        user_id=2,
        task_id=3,
        creation_time=datetime.strptime("11/11/2008 16:10",
                                        get_date_format())
    )

    voice.add_notification(
        user_id=1,
        notification_kind=NotificationKind.NEW_COMMENT,
        description="Some description",
        creation_time=datetime.strptime("11/11/2008 16:10",
                                        get_date_format()),
    )

    return voice


def get_full_filled_voice():
    voice = get_half_filled_voice()

    voice.add_doer(user_id=1, task_id=1)
    voice.add_doer(user_id=1, task_id=2)
    voice.add_doer(user_id=2, task_id=3)

    voice.mark_with_tag(tag_id=1, task_id=2)
    voice.mark_with_tag(tag_id=1, task_id=3)

    voice.add_doer(user_id=1, supertask_id=1)
    voice.add_doer(user_id=2, supertask_id=1)

    voice.add_dependence(1, 2)
    voice.add_dependence(2, 3)

    return voice


def get_date_format():
    date_format = "%d/%m/%Y %H:%M"

    return date_format