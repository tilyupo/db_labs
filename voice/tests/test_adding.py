import unittest

from datetime import datetime, timedelta
from .test_utils import get_empty_voice, get_date_format
from ..models import NotificationKind, SuperTaskKind


class AddingTests(unittest.TestCase):
    def setUp(self):
        self.voice = get_empty_voice()
        self.date_format = get_date_format()

    def test_adding_user(self):
        init_dict = {
            "name": "Anton",
            "email": "thescir@yandex.ru",
            "creation_time": datetime.strptime("12/11/2008 16:10",
                                               self.date_format),
        }

        id = self.voice.add_user(**init_dict)
        obj = self.voice.get_object("user", id)

        self.assertDictContainsSubset(init_dict, obj.__dict__)

    def test_adding_task(self):
        init_dict = {
            "title": "Eat ice-cream",
            "description": "Buy and eat ice_cream from magazine.",
            "deadline": datetime.strptime("12/12/2008 16:10",
                                          self.date_format),
            "creation_time": datetime.strptime("12/11/2008 16:10",
                                               self.date_format),
        }

        id = self.voice.add_task(**init_dict)
        obj = self.voice.get_object("task", id)

        self.assertDictContainsSubset(init_dict, obj.__dict__)

    def test_adding_supertask(self):
        user_id = self.voice.add_user("anton")

        init_dict = {
            "creator_id": user_id,
            "kind": SuperTaskKind.INTERVAL_KIND,
            "title": "Meat",
            "description": "Eat Meat",
            "creation_time": datetime.strptime("11/11/2008 16:10",
                                               self.date_format),
            "first_creation_time": datetime.strptime("12/11/2008 16:10",
                                                     self.date_format),
            "re_creation_period": timedelta(6000),
            "time_to_execution": timedelta(600),
        }

        id = self.voice.add_supertask(**init_dict)
        obj = self.voice.get_object("supertask", id)

        del init_dict["first_creation_time"]
        init_dict["next_creation_time"] = datetime.strptime("12/11/2008 16:10",
                                                            self.date_format)

        self.assertDictContainsSubset(init_dict, obj.__dict__)

    def test_adding_tag(self):
        init_dict = {
            "name": "book",
            "creation_time": datetime.strptime("11/11/2008 16:10",
                                               self.date_format),
        }

        id = self.voice.add_tag(**init_dict)
        obj = self.voice.get_object("tag", id)

        self.assertDictContainsSubset(init_dict, obj.__dict__)

    def test_adding_comment(self):
        task_init_dict = {
            "title": "Eat ice-cream",
            "description": "Buy and eat ice_cream from magazine.",
            "deadline": datetime.strptime("12/12/2008 16:10",
                                          self.date_format),
        }
        task_id = self.voice.add_task(**task_init_dict)
        
        user_init_dict = {
            "name": "Anton",
        }
        user_id = self.voice.add_user(**user_init_dict)

        comment_init_dict = {
            "text": "Some text",
            "creation_time": datetime.strptime("11/11/2008 16:10",
                                               self.date_format),
            "user_id": user_id,
            "task_id": task_id,
        }
        id = self.voice.add_comment(**comment_init_dict)
        obj = self.voice.get_object("comment", id)

        del comment_init_dict["user_id"]
        comment_init_dict["owner_id"] = user_id

        self.assertDictContainsSubset(comment_init_dict, obj.__dict__)

    def test_adding_notification(self):
        user_init_dict = {
            "name": "Anton",
        }
        user_id = self.voice.add_user(**user_init_dict)

        notification_init_dict = {
            "notification_kind": NotificationKind.NEW_COMMENT,
            "description": "Some description",
            "creation_time": datetime.strptime("11/11/2008 16:10",
                                               self.date_format),
            "user_id": user_id,
        }

        id = self.voice.add_notification(**notification_init_dict)
        obj = self.voice.get_object("notification", id)

        del notification_init_dict["notification_kind"]
        notification_init_dict["kind"] = NotificationKind.NEW_COMMENT

        self.assertDictContainsSubset(notification_init_dict, obj.__dict__)

    def voicerDown(self):
        del self.voice
