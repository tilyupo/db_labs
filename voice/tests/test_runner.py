import unittest

from voice.tests.test_adding import AddingTests
from voice.tests.test_completing import CompletingTests
from voice.tests.test_getting import GettingTests
from voice.tests.test_editing import EditingTests
from voice.tests.test_making_connection import ConnectingTests
from voice.tests.test_removing import RemovingTests
from voice.tests.test_synchronization import SynchronizingTests


def get_test_suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(AddingTests))
    suite.addTests(unittest.makeSuite(CompletingTests))
    suite.addTests(unittest.makeSuite(GettingTests))
    suite.addTests(unittest.makeSuite(EditingTests))
    suite.addTests(unittest.makeSuite(ConnectingTests))
    suite.addTests(unittest.makeSuite(RemovingTests))
    suite.addTests(unittest.makeSuite(SynchronizingTests))

    return suite

def run_tests():
    suite = get_test_suite()

    unittest.runner.TextTestRunner(verbosity=2).run(suite)
