import unittest

from datetime import datetime, timedelta
from .test_utils import get_half_filled_voice, get_date_format
from ..models.notification import NotificationKind


class EditingTests(unittest.TestCase):
    def setUp(self):
        self.voice = get_half_filled_voice()
        self.date_format = get_date_format()

    def test_editing_user(self):
        edit_dict = {
            "name": "Bory",
            "email": "thescdfdfir@yandex.ru",
        }
        id = 1
        self.voice.edit_user(id, **edit_dict)
        obj = self.voice.get_object("user", id)

        self.assertDictContainsSubset(edit_dict, obj.__dict__)

    def test_editing_task(self):
        init_dict = {
            "title": "Eat ifdfdce-cream",
            "description": "Buy and eatfdfd ice_cream from magazine.",
            "deadline": datetime.strptime("12/12/1008 16:10",
                                          self.date_format),
            "is_archived": True,
        }

        id = 1
        self.voice.edit_task(id, **init_dict)
        obj = self.voice.get_object("task", id)

        self.assertDictContainsSubset(init_dict, obj.__dict__)

    def test_editing_supertask(self):
        init_dict = {
            "title": "Mdeat",
            "description": "Eaddt Meat",
            "re_creation_period": timedelta(600),
            "time_to_execution": timedelta(6000),
        }

        id = 1
        self.voice.edit_supertask(id, **init_dict)
        obj = self.voice.get_object("supertask", id)

        self.assertDictContainsSubset(init_dict, obj.__dict__)

    def test_editing_tag(self):
        init_dict = {
            "name": "boodfdk",
        }

        id = 1
        self.voice.edit_tag(id, **init_dict)
        obj = self.voice.get_object("tag", id)

        self.assertDictContainsSubset(init_dict, obj.__dict__)

    def test_editing_comment(self):
        init_dict = {
            "text": "Some text",
        }

        id = 1
        self.voice.edit_comment(id, **init_dict)
        obj = self.voice.get_object("comment", id)

        self.assertDictContainsSubset(init_dict, obj.__dict__)

    def test_editing_notification(self):
        init_dict = {
            "kind": NotificationKind.OVERDUE_TASK,
            "description": "Some dfdfdescription",
        }

        id = 1
        self.voice.edit_notification(id, **init_dict)
        obj = self.voice.get_object("notification", id)

        self.assertDictContainsSubset(init_dict, obj.__dict__)

    def voicerDown(self):
        del self.voice
