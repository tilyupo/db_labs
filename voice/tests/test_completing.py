import unittest

from .test_utils import get_full_filled_voice, get_date_format


class CompletingTests(unittest.TestCase):
    def setUp(self):
        self.voice = get_full_filled_voice()
        self.date_format = get_date_format()

        self.parent_task_id = 1
        self.middle_task_id = 2
        self.child_task_id = 3

    def test_chain_completing_and_uncompleting_task(self):
        self.voice.complete_task(self.child_task_id)
        self.voice.complete_task(self.middle_task_id)
        self.voice.complete_task(self.parent_task_id)
        self.assertTrue(
            getattr(
                self.voice.get_object("task", self.child_task_id),
                "is_done",
            )
        )
        self.assertTrue(
            getattr(
                self.voice.get_object("task", self.middle_task_id),
                "is_done",
            )
        )
        self.assertTrue(
            getattr(
                self.voice.get_object("task", self.parent_task_id),
                "is_done",
            )
        )

        self.voice.resume_task(self.parent_task_id)
        self.voice.resume_task(self.middle_task_id)
        self.voice.resume_task(self.child_task_id)
        self.assertFalse(
            getattr(
                self.voice.get_object("task", self.parent_task_id),
                "is_done",
            )
        )
        self.assertFalse(
            getattr(
                self.voice.get_object("task", self.middle_task_id),
                "is_done",
            )
        )
        self.assertFalse(
            getattr(
                self.voice.get_object("task", self.child_task_id),
                "is_done",
            )
        )

    def test_incorrect_completing_task(self):
        with self.assertRaises(ValueError):
            self.voice.complete_task(self.middle_task_id)

    def test_incorrect_uncompleting_task(self):
        self.voice.complete_task(self.child_task_id)
        self.voice.complete_task(self.middle_task_id)

        with self.assertRaises(ValueError):
            self.voice.resume_task(self.child_task_id)

    def test_force_completing_task(self):
        parent_task = self.voice.get_object("task", self.parent_task_id)
        child_task = self.voice.get_object("task", self.child_task_id)

        with self.assertRaises(ValueError):
            self.voice.complete_task(self.parent_task_id, force=False)

        self.voice.complete_task(self.parent_task_id, force=True)
        self.assertTrue(parent_task.is_done)
        self.assertTrue(child_task.is_done)

    def test_force_uncompleting_task(self):
        self.voice.complete_task(self.parent_task_id, force=True)

        parent_task = self.voice.get_object("task", self.parent_task_id)
        child_task = self.voice.get_object("task", self.child_task_id)

        with self.assertRaises(ValueError):
            self.voice.resume_task(self.child_task_id, force=False)

        self.voice.resume_task(self.child_task_id, force=True)
        self.assertFalse(parent_task.is_done)
        self.assertFalse(child_task.is_done)

    def test_invalid_making_dependence(self):
        self.voice.remove_dependence(self.middle_task_id, self.child_task_id)

        self.voice.complete_task(self.middle_task_id)

        with self.assertRaises(ValueError):
            self.voice.add_dependence(self.middle_task_id, self.child_task_id)

    def voicerDown(self):
        del self.voice
