import unittest

from datetime import datetime, timedelta
from .test_utils import get_empty_voice, get_date_format
from ..models import NotificationKind, SuperTaskKind


class SynchronizingTests(unittest.TestCase):
    def setUp(self):
        self.voice = get_empty_voice()
        self.date_format = get_date_format()

        self.some_time = datetime.strptime(
            "12/12/2008 16:11",
            self.date_format,
        )

        self.current_time = datetime.strptime(
            "12/12/2008 16:12",
            self.date_format,
        )

        self.user_id = self.voice.add_user("anton")

    def test_auto_creation_task_while_sync(self):
        step = timedelta(seconds=80)

        supertask_id = self.voice.add_supertask(
            creator_id=self.user_id,
            kind=SuperTaskKind.INTERVAL_KIND,
            description="Some description",
            first_creation_time=self.some_time,
            re_creation_period=step,
        )
        supertask = self.voice.get_object("supertask", supertask_id)

        self.voice.synchronize_time(self.current_time)

        tasks = self.voice.get_tasks()
        self.assertEqual(len(tasks), 1)
        task = tasks[0]

        self.assertEqual(task.creation_time, self.some_time)
        self.assertEqual(
            supertask.next_creation_time,
            self.some_time + step,
        )

        self.assertEqual(task.description, supertask.description)

    def test_auto_marking_task_as_expired(self):
        task_id = self.voice.add_task(
            description="Some description.",
            deadline=self.some_time,
        )
        task = self.voice.get_object("task", task_id)

        self.assertFalse(task.is_expired)
        self.voice.synchronize_time(self.current_time)
        self.assertTrue(task.is_expired)

    def test_auto_generating_notification(self):
        task_id = self.voice.add_task(
            description="Some description.",
            deadline=self.some_time,
        )
        user_id = self.voice.add_user(
            name="Anton",
        )

        self.voice.add_doer(
            user_id=user_id,
            task_id=task_id,
        )

        self.voice.synchronize_time(
            current_time=self.current_time,
            generate_notifications=True,
        )

        notifications = self.voice.get_notifications(user_ids=[user_id])
        self.assertEqual(len(notifications), 1)
        notification = notifications[0]

        self.assertEqual(notification.kind, NotificationKind.OVERDUE_TASK)

    def voicerDown(self):
        del self.voice
