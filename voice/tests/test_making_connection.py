import unittest

from voice.tests.test_utils import get_half_filled_voice, get_date_format
from voice.exceptions import CicleDependenceError


class ConnectingTests(unittest.TestCase):
    def setUp(self):
        self.voice = get_half_filled_voice()
        self.date_format = get_date_format()

        self.user_id = 1
        self.task_id = 1
        self.supertask_id = 1
        self.tag_id = 1
        self.comment_id = 1
        self.notification_id = 1

        self.child_task_id = 1
        self.middle_task_id = 2
        self.parent_task_id = 3

    # region task_user_connection
    def test_task_user_doer_connection(self):
        task = self.voice.get_object("task", self.task_id)
        user = self.voice.get_object("user", self.user_id)
        self.assertNotIn(user, task.doers)

        self.voice.add_doer(
            user_id=self.user_id,
            task_id=self.task_id,
        )
        self.assertIn(user, task.doers)

        self.voice.remove_doer(
            user_id=self.user_id,
            task_id=self.task_id,
        )
        self.assertNotIn(user, task.doers)

    def test_task_user_moderator_connection(self):
        task = self.voice.get_object("task", self.task_id)
        user = self.voice.get_object("user", self.user_id)
        self.assertNotIn(user, task.moderators)

        self.voice.add_moderator(
            user_id=self.user_id,
            task_id=self.task_id,
        )
        self.assertIn(user, task.moderators)

        self.voice.remove_moderator(
            user_id=self.user_id,
            task_id=self.task_id,
        )
        self.assertNotIn(user, task.moderators)

    def test_task_user_watcher_connection(self):
        task = self.voice.get_object("task", self.task_id)
        user = self.voice.get_object("user", self.user_id)
        self.assertNotIn(user, task.watchers)

        self.voice.add_watcher(
            user_id=self.user_id,
            task_id=self.task_id,
        )
        self.assertIn(user, task.watchers)

        self.voice.remove_watcher(
            user_id=self.user_id,
            task_id=self.task_id,
        )
        self.assertNotIn(user, task.watchers)
    # endregion

    def test_task_comment_connection(self):
        comment = self.voice.get_object("comment", self.comment_id)

        other_task = self.voice.get_object("task", 1)
        self.assertNotEqual(other_task, comment.task)

        connected_task = self.voice.get_object("task", 3)
        self.assertEqual(connected_task, comment.task)

    def test_task_tag_connection(self):
        task = self.voice.get_object("task", self.task_id)
        tag = self.voice.get_object("tag", self.tag_id)
        self.assertNotIn(task, tag.tasks)

        self.voice.mark_with_tag(
            tag_id=self.tag_id,
            task_id=self.task_id,
        )
        self.assertIn(task, tag.tasks)

        self.voice.unmark_with_tag(
            tag_id=self.tag_id,
            task_id=self.task_id,
        )
        self.assertNotIn(task, tag.tasks)

    def test_supertask_tag_connection(self):
        supertask = self.voice.get_object("supertask", self.supertask_id)
        tag = self.voice.get_object("tag", self.tag_id)
        self.assertNotIn(supertask, tag.supertasks)

        self.voice.mark_with_tag(
            tag_id=self.tag_id,
            supertask_id=self.supertask_id,
        )
        self.assertIn(supertask, tag.supertasks)

        self.voice.unmark_with_tag(
            tag_id=self.tag_id,
            supertask_id=self.supertask_id,
        )
        self.assertNotIn(supertask, tag.supertasks)

    # region supertask_user_connection
    def test_supertask_user_doer_connection(self):
        supertask = self.voice.get_object("supertask", self.supertask_id)
        user = self.voice.get_object("user", self.user_id)
        self.assertNotIn(user, supertask.doers)

        self.voice.add_doer(
            user_id=self.user_id,
            supertask_id=self.supertask_id,
        )
        self.assertIn(user, supertask.doers)

        self.voice.remove_doer(
            user_id=self.user_id,
            supertask_id=self.supertask_id,
        )
        self.assertNotIn(user, supertask.doers)

    def test_supertask_user_moderator_connection(self):
        supertask = self.voice.get_object("supertask", self.supertask_id)
        user = self.voice.get_object("user", self.user_id)
        self.assertNotIn(user, supertask.moderators)

        self.voice.add_moderator(
            user_id=self.user_id,
            supertask_id=self.supertask_id,
        )
        self.assertIn(user, supertask.moderators)

        self.voice.remove_moderator(
            user_id=self.user_id,
            supertask_id=self.supertask_id,
        )
        self.assertNotIn(user, supertask.moderators)

    def test_supertask_user_watcher_connection(self):
        supertask = self.voice.get_object("supertask", self.supertask_id)
        user = self.voice.get_object("user", self.user_id)
        self.assertNotIn(user, supertask.watchers)

        self.voice.add_watcher(
            user_id=self.user_id,
            supertask_id=self.supertask_id,
        )
        self.assertIn(user, supertask.watchers)

        self.voice.remove_watcher(
            user_id=self.user_id,
            supertask_id=self.supertask_id,
        )
        self.assertNotIn(user, supertask.watchers)
    # endregion

    def test_user_notification_connection(self):
        notification = self.voice.get_object("notification",
                                           self.notification_id)
        user = self.voice.get_object("user", self.user_id)
        self.assertIn(notification, user.notifications)

    def test_user_comment_connection(self):
        comment = self.voice.get_object("comment", self.comment_id)

        other_user = self.voice.get_object("user", 1)
        self.assertNotIn(comment, other_user.comments)

        other_user = self.voice.get_object("user", 2)
        self.assertIn(comment, other_user.comments)

    def test_no_kwargs_in_connection(self):
        with self.assertRaises(ValueError):
            self.voice.add_doer(
                user_id=self.user_id,
            )

        with self.assertRaises(ValueError):
            self.voice.remove_doer(
                user_id=self.user_id,
            )

        with self.assertRaises(ValueError):
            self.voice.mark_with_tag(
                tag_id=self.tag_id,
            )

        with self.assertRaises(ValueError):
            self.voice.unmark_with_tag(
                tag_id=self.tag_id,
            )

    def test_multi_connection(self):
        self.voice.add_doer(
            task_id=self.task_id,
            user_id=self.user_id,
        )
        with self.assertRaises(ValueError):
            self.voice.add_doer(
                task_id=self.task_id,
                user_id=self.user_id,
            )

    def test_removing_not_existed_connection(self):
        with self.assertRaises(ValueError):
            self.voice.remove_moderator(
                task_id=self.task_id,
                user_id=self.user_id,
            )

        with self.assertRaises(ValueError):
            self.voice.remove_doer(
                task_id=self.task_id,
                user_id=self.user_id,
            )

        with self.assertRaises(ValueError):
            self.voice.remove_watcher(
                task_id=self.task_id,
                user_id=self.user_id,
            )

    def test_task_dependence(self):
        child_task = self.voice.get_object("task", self.child_task_id)
        parent_task = self.voice.get_object("task", self.parent_task_id)
        self.assertNotIn(child_task, parent_task.subtasks)

        self.voice.add_dependence(self.parent_task_id, self.child_task_id)
        self.assertIn(child_task, parent_task.subtasks)

        self.voice.remove_dependence(self.parent_task_id, self.child_task_id)
        self.assertNotIn(child_task, parent_task.subtasks)

    def test_multi_dependence(self):
        self.voice.add_dependence(self.parent_task_id, self.child_task_id)
        with self.assertRaises(ValueError):
            self.voice.add_dependence(self.parent_task_id, self.child_task_id)

    def test_removing_not_existed_dependence(self):
        with self.assertRaises(ValueError):
            self.voice.remove_dependence(self.parent_task_id, self.child_task_id)

    def test_cicle_dependence1(self):
        self.voice.add_dependence(self.parent_task_id, self.child_task_id)
        with self.assertRaises(CicleDependenceError):
            self.voice.add_dependence(self.child_task_id, self.parent_task_id)

    def test_cicle_dependence2(self):
        self.voice.add_dependence(self.parent_task_id, self.middle_task_id)
        self.voice.add_dependence(self.middle_task_id, self.child_task_id)
        with self.assertRaises(CicleDependenceError):
            self.voice.add_dependence(self.child_task_id, self.parent_task_id)

    def test_user_trusting(self):
        anton = self.voice.get_users(names=["Anton"])[0]
        alex = self.voice.get_users(names=["Alex"])[0]

        self.assertNotIn(alex, anton.trustings)
        self.assertNotIn(anton, alex.trustings)

        self.voice.add_trust(anton.id, alex.id)
        self.assertIn(alex, anton.trustings)

        self.voice.remove_trust(anton.id, alex.id)
        self.assertNotIn(alex, anton.trustings)

    def test_multi_trusting(self):
        anton = self.voice.get_users(names=["Anton"])[0]
        alex = self.voice.get_users(names=["Alex"])[0]

        self.voice.add_trust(alex.id, anton.id)

        with self.assertRaises(ValueError):
            self.voice.add_trust(alex.id, anton.id)

    def test_multi_untrusting(self):
        anton = self.voice.get_users(names=["Anton"])[0]
        alex = self.voice.get_users(names=["Alex"])[0]

        self.assertNotIn(alex, anton.trustings)

        with self.assertRaises(ValueError):
            self.voice.remove_trust(anton.id, alex.id)

    def voicerDown(self):
        del self.voice
