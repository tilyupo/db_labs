import unittest

from datetime import datetime
from voice.utils import get_ids
from voice.tests.test_utils import (
    get_empty_voice,
    get_full_filled_voice,
    get_date_format
)


class GettingTests(unittest.TestCase):
    def setUp(self):
        self.voice = get_full_filled_voice()
        self.date_format = get_date_format()

    def test_getting_user(self):
        users = self.voice.get_users()
        self.assertEqual(len(users), 2)
        self.assertCountEqual(get_ids(users), [1, 2])

        users = self.voice.get_users(
            task_ids=[1, 2],
        )
        self.assertEqual(len(users), 1)
        self.assertEqual(users[0].id, 1)

        users = self.voice.get_users(
            user_ids=[1],
            task_ids=[3],
        )
        self.assertEqual(len(users), 0)

        users = self.voice.get_users(
            user_ids=[2],
            supertask_ids=[1],
        )
        self.assertEqual(len(users), 1)
        self.assertEqual(users[0].id, 2)

        users = self.voice.get_users(
            names=["Anton"]
        )
        self.assertEqual(len(users), 1)
        self.assertEqual(users[0].name, "Anton")

    @unittest.skip("Old version of get_tasks")
    def test_getting_task_v1(self):
        tasks = self.voice.get_tasks()
        self.assertEqual(len(tasks), 3)
        self.assertCountEqual(get_ids(tasks), [1, 2, 3])

        tasks = self.voice.get_tasks(
            user_ids=[1, 2],
        )
        self.assertEqual(len(tasks), 3)
        self.assertCountEqual(get_ids(tasks), [1, 2, 3])

        tasks = self.voice.get_tasks(
            user_ids=[1],
            tag_ids=[1],
        )
        self.assertEqual(len(tasks), 1)
        self.assertEqual(tasks[0].id, 2)

        tasks = self.voice.get_tasks(
            task_ids=[1],
            tag_ids=[1],
        )
        self.assertEqual(len(tasks), 0)

    def test_getting_tasks_v2(self):
        self.voice = get_empty_voice()

        # adding user
        first_user_id = self.voice.add_user("first")
        second_user_id = self.voice.add_user("second")

        # adding task
        deadline = datetime(1999, 10, 18)
        first_task_id = self.voice.add_task(
            description="description",
            deadline=deadline,
        )
        second_task_id = self.voice.add_task(
            description="description",
            deadline=deadline,
        )
        third_task_id = self.voice.add_task(
            description="description",
            deadline=deadline,
        )

        # setting creator
        self.voice.set_creator(user_id=first_user_id, task_id=first_task_id)

        # setting doers
        self.voice.add_doer(user_id=second_user_id, task_id=third_task_id)
        self.voice.add_moderator(user_id=second_user_id, task_id=second_task_id)
        self.voice.add_watcher(user_id=first_user_id, task_id=third_task_id)

        # checking queries
        tasks = self.voice.get_tasks()
        self.assertEqual(len(tasks), 3)

        tasks = self.voice.get_tasks(user_ids=[first_user_id])
        self.assertEqual(len(tasks), 2)
        self.assertIn(first_task_id, get_ids(tasks))
        self.assertIn(third_task_id, get_ids(tasks))

        tasks = self.voice.get_tasks(user_ids=[second_user_id])
        self.assertEqual(len(tasks), 2)
        self.assertIn(second_task_id, get_ids(tasks))
        self.assertIn(third_task_id, get_ids(tasks))

        tasks = self.voice.get_tasks(user_ids=[first_user_id, second_user_id])
        self.assertEqual(len(tasks), 3)

    def test_getting_supertask(self):
        supertasks = self.voice.get_supertasks()
        self.assertEqual(len(supertasks), 1)

        supertasks = self.voice.get_supertasks(
            user_ids=[1, 2],
        )
        self.assertEqual(len(supertasks), 1)

        supertasks = self.voice.get_supertasks(
            user_ids=[1],
            tag_ids=[1],
        )
        self.assertEqual(len(supertasks), 0)

    def test_getting_tag(self):
        tags = self.voice.get_tags()
        self.assertEqual(len(tags), 1)

        tags = self.voice.get_tags(
            task_ids=[1, 2],
        )
        self.assertEqual(len(tags), 1)
        self.assertEqual(tags[0].id, 1)

        tags = self.voice.get_tags(
            tag_ids=[1],
            task_ids=[1],
        )
        self.assertEqual(len(tags), 0)

        tags = self.voice.get_tags(
            supertask_ids=[1],
        )
        self.assertEqual(len(tags), 0)

        tags = self.voice.get_tags(
            names=["book"]
        )
        self.assertEqual(len(tags), 1)
        self.assertEqual(tags[0].name, "book")

    def test_getting_comment(self):
        comments = self.voice.get_comments()
        self.assertEqual(len(comments), 1)

        comments = self.voice.get_comments(
            task_ids=[1, 3],
        )
        self.assertEqual(len(comments), 1)
        self.assertEqual(comments[0].id, 1)

        comments = self.voice.get_comments(
            comment_ids=[1],
            task_ids=[1],
        )
        self.assertEqual(len(comments), 0)

        comments = self.voice.get_comments(
            user_ids=[1]
        )
        self.assertEqual(len(comments), 0)

    def test_getting_notification(self):
        notifications = self.voice.get_notifications()
        self.assertEqual(len(notifications), 1)

        notifications = self.voice.get_notifications(
            user_ids=[1],
        )
        self.assertEqual(len(notifications), 1)
        self.assertEqual(notifications[0].id, 1)

        notifications = self.voice.get_notifications(
            user_ids=[2],
        )
        self.assertEqual(len(notifications), 0)

        notifications = self.voice.get_notifications(
            notification_ids=[2],
        )
        self.assertEqual(len(notifications), 0)

    def voicerDown(self):
        del self.voice
