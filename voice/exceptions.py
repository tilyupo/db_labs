"""
    Library specific errors
"""


class EntityDontExistsError(Exception):
    """Entity do not exists in store error"""
    pass


class ObjectDontExistsError(Exception):
    """Entity do not exists in store error"""
    pass


class CicleDependenceError(Exception):
    """Command do not allowed or used incorectlly error"""
    def __init__(self, subtask_id, overtask_id):
        self.subtask_id = subtask_id
        self.overtask_id = overtask_id

        super(CicleDependenceError, self).__init__()

    def __str__(self):
        return "Dependence Task {} from Task {} leads to cicle dependence".format(
            self.overtask_id,
            self.subtask_id,
        )
