class Blog:
    def __init__(self, id=None, title=None, creator_id=None):

        self.id = id
        self.title = title
        self.creator_id = creator_id

    @staticmethod
    def get_headers(without_id=False):
        headers = [
            "id",
            "title",
            "creator_id"
        ]

        if without_id:
            headers.remove("id")

        return headers
