class Post:
    def __init__(self,
                 id=None,
                 title=None,
                 content=None,
                 category_id=None,
                 blog_id=None):

        self.id = id
        self.title = title
        self.content = content
        self.blog_id = blog_id
        self.category_id = category_id

    @staticmethod
    def get_headers(without_id=False):
        headers = [
            "id",
            "title",
            "content",
            "category_id",
            "blog_id"
        ]

        if without_id:
            headers.remove("id")

        return headers
