class Tag:
    def __init__(self, id=None, name=None):

        self.id = id
        self.name = name

    @staticmethod
    def get_headers(without_id=False):
        headers = [
            "id",
            "name",
        ]

        if without_id:
            headers.remove("id")

        return headers
