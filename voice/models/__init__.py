"""
    Containe all entities for working Voice.
"""
from voice.models.blog import Blog
from voice.models.post import Post
from voice.models.user import User
from voice.models.fake import Fake
from voice.models.comment import Comment
from voice.models.tag import Tag
from voice.models.category import Category

__all__ = ['Blog', 'Post', 'User', 'Fake', 'Comment', 'Tag', 'Category']
