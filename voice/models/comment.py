class Comment:
    def __init__(self, id=None, content=None,
                 creator_id=None, post_id=None):

        self.id = id
        self.content = content
        self.creator_id = creator_id
        self.post_id = post_id

    @staticmethod
    def get_headers(without_id=False):
        headers = [
            "id",
            "content",
            "creator_id",
            "post_id",
        ]

        if without_id:
            headers.remove("id")

        return headers
