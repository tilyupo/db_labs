class User:
    def __init__(self, id=None, login=None, password_hash=None):

        self.id = id
        self.login = login
        self.password_hash = None

    @staticmethod
    def get_headers(without_id=False):
        headers = [
            "id",
            "login",
            "password_hash",
        ]

        if without_id:
            headers.remove("id")

        return headers
