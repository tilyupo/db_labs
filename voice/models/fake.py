class Fake:
    def __init__(self, id=None, nickname=None, user_id=None):

        self.id = id
        self.nickname = nickname
        self.user_id = user_id

    @staticmethod
    def get_headers(without_id=False):
        headers = [
            "id",
            "nickname",
            "user_id",
        ]

        if without_id:
            headers.remove("id")

        return headers
