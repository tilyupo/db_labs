"""
    Voice lib containe models, main-core class for
    bisness logic, tests for checking state of library.
    SQLAlchemy specific entities already implemented
    in 'models' module.

    The main object of library is class voice.voice.Voice.
    See it's documentation for example of usage.
"""

from voice.voice import Voice
