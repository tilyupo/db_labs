from django.shortcuts import render
from django.shortcuts import redirect, render
from django.urls import reverse

from voice_app.context import create_ctx
from voice_app.forms import *
from voice_cli.permission_utils import pass_current_user_id_factory
from voice_cli.cli_utils.cli_voice import cli_voice
from voice_cli.presenter_utils import user2str, fake2str
from voice_cli.error_handling import error_handler_wrapper
from voice_cli.config_utils import change_current_user, get_default_config_path, change_current_fake

from voice_cli.permission_utils import (pass_current_user_id)
from voice_cli.cli_utils.cli_voice import cli_voice
from voice_cli.presenter_utils import blog2str
from voice_cli.error_handling import error_handler_wrapper
from voice.models import *


def index(request):
    return render(request, 'voice_app/index.html')


def relogin(request):
    if request.method == 'POST':
        form = ReloginForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['login']

            config_path = get_default_config_path()
            change_current_user(config_path, username)

            return redirect(reverse('voice_app:index'))
    else:
        form = ReloginForm()

    return render(request, 'voice_app/relogin.html', {'form': form})


@pass_current_user_id_factory(create_ctx)
def user_info(user_id, fake_id, request):
    ctx = create_ctx()

    return render(request, 'voice_app/current_user.html', {
        'username': ctx.voice.user_service.select([user_id])[0],
        'fake': ctx.voice.fake_service.select([fake_id])[0]
    })


@pass_current_user_id_factory(create_ctx)
def add_blog(user_id, fake_id, request):
    ctx = create_ctx()

    if request.method == 'POST':
        form = AddBlogForm(request.POST)

        if form.is_valid():
            title = form.cleaned_data['title']

            voice = ctx.voice

            blog = Blog(title=title, creator_id=fake_id)

            voice.blog_service.add(blog)

            return redirect(reverse('voice_app:index'))
    else:
        form = AddBlogForm()

    return render(request, 'voice_app/add_blog.html', {'form': form})


@pass_current_user_id_factory(create_ctx)
def show_blog(user_id, fake_id, request):
    ctx = create_ctx()

    blog_id = int(request.GET['blog_id'])

    voice = ctx.voice
    print("blog_id:", blog_id)

    blogs = voice.blog_service.select([blog_id])
    print("blogs_len:", len(blogs))
    if len(blogs) == 0:
        print("Object with this id is not exists")
        return 0

    blog = blogs[0]

    blogs = voice.blog_service.select([blog_id])

    posts = voice.post_service.select_by_blog(blog_id)

    return render(request, 'voice_app/show_blog.html', {'blog': blog, 'posts': posts, 'user_id': user_id})


@pass_current_user_id_factory(create_ctx)
def blog_list(user_id, fake_id, request):
    ctx = create_ctx()

    voice = ctx.voice

    blogs = voice.blog_service.get_all()

    return render(request, 'voice_app/blog_list.html', {'blogs': blogs, 'user_id': user_id})


@pass_current_user_id_factory(create_ctx)
def edit_blog(user_id, fake_id, request):
    ctx = create_ctx()

    blog_id = int(request.GET['blog_id'])

    if request.method == 'POST':
        form = EditBlogForm(request.POST)

        if form.is_valid():
            title = form.cleaned_data['title']

            voice = ctx.voice

            blogs = voice.blog_service.select([blog_id])

            blog = blogs[0]
            blog.title = title
            voice.blog_service.update(blog)

            return redirect(reverse('voice_app:show_blog') + '?blog_id={}'.format(blog_id))
    else:
        form = EditBlogForm()

    return render(request, 'voice_app/edit_blog.html', {'form': form})


def remove_blog(request):
    ctx = create_ctx()

    blog_id = int(request.GET['blog_id'])

    voice = ctx.voice

    blogs = voice.blog_service.select([blog_id])

    blog = blogs[0]
    voice.blog_service.remove([blog.id])

    return redirect(reverse('voice_app:index'))


def add_cat(request):
    ctx = create_ctx()

    if request.method == 'POST':
        form = AddCatForm(request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']

            voice = ctx.voice

            category = Category(name=name, )

            voice.category_service.add(category)

            return redirect(reverse('voice_app:index'))
    else:
        form = AddCatForm()

    return render(request, 'voice_app/add_cat.html', {'form': form})


def all_cats(request):
    ctx = create_ctx()

    voice = ctx.voice

    categorys = voice.category_service.select_all()

    return render(request, 'voice_app/all_cats.html', {'cats': categorys})


def edit_cat(request):
    ctx = create_ctx()

    category_id = int(request.GET['cat_id'])

    if request.method == 'POST':
        form = EditCatForm(request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']

            voice = ctx.voice

            categorys = voice.category_service.select([category_id])

            category = categorys[0]
            category.name = name
            voice.category_service.update(category)

            return redirect(reverse('voice_app:index'))
    else:
        form = EditCatForm()

    return render(request, 'voice_app/edit_cat.html', {'form': form})


def remove_cat(request):
    ctx = create_ctx()

    category_id = int(request.GET['cat_id'])

    voice = ctx.voice

    categorys = voice.category_service.select([category_id])

    category = categorys[0]
    voice.category_service.remove([category.id])

    return redirect(reverse('voice_app:index'))


@pass_current_user_id_factory(create_ctx)
def add_comment(user_id, fake_id, request):
    ctx = create_ctx()

    post_id = int(request.GET['post_id'])

    if request.method == 'POST':
        form = AddCommentForm(request.POST)

        if form.is_valid():
            content = form.cleaned_data['content']

            voice = ctx.voice

            comment = Comment(
                content=content,
                creator_id=fake_id,
                post_id=post_id,
            )

            voice.comment_service.add(comment)

            return redirect(reverse('voice_app:show_post') + '?post_id={}'.format(post_id))
    else:
        form = AddCommentForm()

    return render(request, 'voice_app/add_comment.html', {'form': form})


@pass_current_user_id_factory(create_ctx)
def edit_comment(user_id, fake_id, request):
    ctx = create_ctx()

    comment_id = int(request.GET['comment_id'])

    if request.method == 'POST':
        form = EditCommentForm(request.POST)

        if form.is_valid():
            content = form.cleaned_data['content']

            voice = ctx.voice

            comments = voice.comment_service.select([comment_id])

            comment = comments[0]
            comment.content = content
            voice.comment_service.update(comment)

            return redirect(reverse('voice_app:all_comments') + '?post_id={}'.format(comment.post_id))
    else:
        form = EditCommentForm()

    return render(request, 'voice_app/edit_comment.html', {'form': form})


@pass_current_user_id_factory(create_ctx)
def remove_comment(user_id, fake_id, request):
    ctx = create_ctx()

    comment_id = int(request.GET['comment_id'])

    voice = ctx.voice

    comments = voice.comment_service.select([comment_id])

    comment = comments[0]
    voice.comment_service.remove([comment.id])

    return redirect(reverse('voice_app:show_post') + '?post_id={}'.format(comment.post_id))


@pass_current_user_id_factory(create_ctx)
def all_comments(user_id, fake_id, request):
    ctx = create_ctx()

    post_id = int(request.GET['post_id'])

    voice = ctx.voice

    comments = voice.comment_service.select_by_post(post_id)
    fakes = voice.fake_service.select([comment.creator_id for comment in comments])

    for comment in comments:
        comment.nickname = next(fake.nickname for fake in fakes if fake.id == comment.creator_id)

    return render(request, 'voice_app/comments.html', {
        'comments': comments,
        'user_id': fake_id,
    })


@pass_current_user_id_factory(create_ctx)
def add_post(user_id, fake_id, request):
    ctx = create_ctx()

    blog_id = int(request.GET['blog_id'])

    if request.method == 'POST':
        form = AddPostForm(request.POST)

        if form.is_valid():
            title = form.cleaned_data['title']
            content = form.cleaned_data['content']
            category_id = int(form.cleaned_data['category_id'])
            """Create post from current fake of user."""
            voice = ctx.voice

            post = Post(
                title=title,
                content=content,
                blog_id=blog_id,
                category_id=category_id,
            )

            voice.post_service.add(post)

            return redirect(reverse('voice_app:show_blog') + '?blog_id={}'.format(blog_id))
    else:
        form = AddPostForm()

    return render(request, 'voice_app/add_post.html', {'form': form})


@pass_current_user_id_factory(create_ctx)
def edit_post(user_id, fake_id, request):
    ctx = create_ctx()

    post_id = int(request.GET['post_id'])

    if request.method == 'POST':
        form = EditPostForm(request.POST)

        if form.is_valid():
            title = form.cleaned_data['title']
            content = form.cleaned_data['content']
            category_id = int(form.cleaned_data['category_id'])

            voice = ctx.voice

            posts = voice.post_service.select([post_id])

            post = posts[0]
            post.title = title
            post.content = content
            post.category_id = category_id

            voice.post_service.update(post)

            return redirect(reverse('voice_app:show_post') + '?post_id_id={}'.format(post_id))
    else:
        form = EditPostForm()

    return render(request, 'voice_app/edit_post.html', {'form': form})


@pass_current_user_id_factory(create_ctx)
def show_post(user_id, fake_id, request):
    ctx = create_ctx()

    post_id = int(request.GET['post_id'])

    voice = ctx.voice

    posts = voice.post_service.select([post_id])
    post = posts[0]

    return render(request, 'voice_app/show_post.html', {'post': post})


@pass_current_user_id_factory(create_ctx)
def remove_post(user_id, fake_id, request):
    ctx = create_ctx()

    post_id = int(request.GET['post_id'])

    voice = ctx.voice

    posts = voice.post_service.select([post_id])
    post = posts[0]

    voice.post_service.remove([post.id])

    return redirect(reverse('voice_app:show_post') + '?post_id_id={}'.format(post_id))


def change_nickname(request):
    if request.method == 'POST':
        form = ChangeNickForm(request.POST)

        if form.is_valid():
            nickname = form.cleaned_data['nickname']

            config_path = get_default_config_path()
            change_current_fake(config_path, nickname)

            return redirect(reverse('voice_app:index'))
    else:
        form = ChangeNickForm()

    return render(request, 'voice_app/change_nickname.html', {'form': form})
