from django.conf.urls import url

from voice_app import views

app_name = 'voice_app'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^relogin$', views.relogin, name='relogin'),
    url(r'^userinfo$', views.user_info, name='userinfo'),
    url(r'^add_blog$', views.add_blog, name='add_blog'),
    url(r'^show_blog$', views.show_blog, name='show_blog'),
    url(r'^blog_list$', views.blog_list, name='blog_list'),
    url(r'^edit_blog$', views.edit_blog, name='edit_blog'),
    url(r'^remove_blog$', views.remove_blog, name='remove_blog'),
    url(r'^add_cat$', views.add_cat, name='add_cat'),
    url(r'^all_cats$', views.all_cats, name='all_cats'),
    url(r'^edit_cat$', views.edit_cat, name='edit_cat'),
    url(r'^remove_cat$', views.remove_cat, name='remove_cat'),
    url(r'^add_comment$', views.add_comment, name='add_comment'),
    url(r'^edit_comment$', views.edit_comment, name='edit_comment'),
    url(r'^remove_comment$', views.remove_comment, name='remove_comment'),
    url(r'^all_comments$', views.all_comments, name='all_comments'),
    url(r'^all_comments$', views.all_comments, name='all_comments'),
    url(r'^show_post$', views.show_post, name='show_post'),
    url(r'^add_post$', views.add_post, name='add_post'),
    url(r'^remove_post$', views.remove_post, name='remove_post'),
    url(r'^edit_post$', views.edit_post, name='edit_post'),
    url(r'^change_nickname$', views.change_nickname, name='change_nickname'),

    url(r'^edit_blog$', views.edit_blog, name='edit_blog'),
    url(r'^edit_blog$', views.edit_blog, name='edit_blog'),
    url(r'^edit_blog$', views.edit_blog, name='edit_blog'),
]