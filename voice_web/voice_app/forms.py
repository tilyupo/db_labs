from django import forms


class BaseFrom(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


class TaskForm(BaseFrom):
    title = forms.CharField(max_length=100)
    tags = forms.CharField(label='Tags, divided by comma', max_length=500, required=False)
    date = forms.DateField(initial=None, widget=forms.SelectDateWidget, required=False)
    days_interval = forms.IntegerField(max_value=5000, min_value=1, initial=None, required=False)


class ReloginForm(BaseFrom):
    login = forms.CharField(max_length=100)


class AddBlogForm(BaseFrom):
    title = forms.CharField(max_length=300)


class EditBlogForm(BaseFrom):
    title = forms.CharField(max_length=300)


class AddCatForm(BaseFrom):
    name = forms.CharField(max_length=300)


class EditCatForm(BaseFrom):
    name = forms.CharField(max_length=300)


class AddCommentForm(BaseFrom):
    content = forms.CharField(max_length=300)


class EditCommentForm(BaseFrom):
    content = forms.CharField(max_length=300)


class AddPostForm(BaseFrom):
    title = forms.CharField(max_length=300)
    content = forms.CharField(max_length=3000)
    category_id = forms.CharField()


class EditPostForm(BaseFrom):
    title = forms.CharField(max_length=300)
    content = forms.CharField(max_length=3000)
    category_id = forms.CharField()


class ChangeNickForm(BaseFrom):
    nickname = forms.CharField(max_length=100)
