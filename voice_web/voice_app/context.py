import warnings
import os

from voice import Voice
from voice_cli.config_utils import get_default_config_path, get_settings_from_config
from collections import namedtuple


def check_config_existence():
    config_path = get_default_config_path()
    if not os.path.exists(config_path):
        raise Exception("Config not found. May be config has been removed..."
                        " If you have no ideas. Try reinstall Voice.")


def create_ctx():
    print('run')

    check_config_existence()
    config_path = get_default_config_path()
    settings = get_settings_from_config(config_path)

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        voice = Voice(settings.connection_string)

    ctx = namedtuple("items", ["voice", "settings"])
    ctx.voice = voice
    ctx.settings = settings

    return ctx
