import os
import configparser

from voice_cli.config_utils import get_default_config_path

from setuptools import find_packages, setup
from setuptools.command.install import install


class InstallCommand(install):
    """Customized setuptools install command - inits configuration file."""

    def run(self):
        # init configuration file
        config = configparser.ConfigParser()
        config.read('voice_cli/default_configuration.ini')

        init_voice_directory()
        with open(get_default_config_path(), 'w') as f:
            config.write(f)

        super().run()


def init_voice_directory():
    dir_path = os.path.join(os.path.expanduser('~'), '.voice')
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)


with open('requirements.txt') as f:
    requirements = list(f)

setup(
    cmdclass={'install': InstallCommand},
    name='voice',
    version='1.0',
    packages=find_packages(),
    package_dir={
        './voice': 'voice',
        './voice_cli': 'voice_cli',
    },
    # test_suite="voice_lib.tests.test_runner.get_test_suite",

    #    long_description=open(join(dirname(__file__), 'readme.md')).read(),
    entry_points={
        'console_scripts': [
            'voice = voice_cli.cli_utils.cli_voice:cli_voice',
        ],
    },
    install_requires=requirements)
