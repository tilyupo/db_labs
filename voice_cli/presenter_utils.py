from voice.models import (
    Blog,
    Post,
    User,
    Fake,
    Tag,
    Category,
    Comment,
)
from typing import (
    List,
    Any,
    Mapping,
    Tuple,
)


def get_full_string(lines: Mapping[str, Tuple[str, str]], need_lines: List[str], sep: str, verbose: int):
    choised_lines = [lines[key] for key in need_lines]

    # expected, that verbose equal 1 means: you want to see
    # minimal information, which fit into one line in console.
    if verbose == 1:
        # So in this way, needn't use meta-wrapper for field
        choised_strings = [str(line[1]) for line in choised_lines]
    else:
        choised_strings = [line[0].format(line[1]) for line in choised_lines]

    result_string = sep.join(choised_strings)

    return result_string


def datetime_as_string(obj_datetime: Any, date_format: str):
    """
        Convert passed 'obj_datetime' to a string with 'date_format' and
        return it.
        If 'obj_datetime' is None, return 'None'
    """
    if obj_datetime is None:
        return str(None)

    return obj_datetime.strftime(date_format)


def get_name(item):
    if item is None:
        return None
    else:
        return item.name


def blog2str(blog: Blog, verbose: int = None):
    lines = dict()
    lines["type"] = ("{}", "Blog")
    lines["id"] = (" ID: {}", blog.id)
    lines["title"] = (" Title: {}", blog.title)
    lines["empty"] = (" {}", "")

    if verbose == 1:
        sep = ", "
        need_lines = "type id title".split()
    elif verbose == 2:
        sep = "\n"
        need_lines = ("type id title empty").split()
    else:
        raise ValueError("Verbose must be in interval [1, 2]")

    return get_full_string(lines, need_lines, sep, verbose)


def post2str(post: Post, verbose: int = None):
    lines = dict()
    lines["type"] = ("{}", "Post")
    lines["id"] = (" ID: {}", post.id)
    lines["title"] = (" Title: {}", post.title)
    lines["empty"] = (" {}", "")
    lines["content"] = (" Content: {}", post.content)
    lines["category_id"] = (" Category: {}", post.category_id)
    lines["blog_id"] = (" Blog: {}", post.blog_id)

    if verbose == 1:
        sep = ", "
        need_lines = "type id title".split()
    elif verbose == 2:
        sep = "\n"
        need_lines = ("type id title content category_id blog_id empty").split()
    else:
        raise ValueError("Verbose must be in interval [1, 2]")

    return get_full_string(lines, need_lines, sep, verbose)


def fake2str(fake: Fake, verbose: int = None):
    lines = dict()
    lines["type"] = ("{}", "Fake")
    lines["id"] = (" ID: {}", fake.id)
    lines["nickname"] = (" Nickname: {}", fake.nickname)
    lines["empty"] = (" {}", "")

    if verbose == 1:
        sep = ", "
        need_lines = "type id nickname".split()
    elif verbose == 2:
        sep = "\n"
        need_lines = ("type id nickname empty").split()
    else:
        raise ValueError("Verbose must be in interval [1, 2]")

    return get_full_string(lines, need_lines, sep, verbose)


def user2str(user: User, verbose: int = None):
    lines = dict()
    lines["type"] = ("{}", "User")
    lines["id"] = (" ID: {}", user.id)
    lines["login"] = (" Login: {}", user.login)
    lines["passwd_hash"] = (" Password hash: {}", user.password_hash)
    lines["empty"] = (" {}", "")

    if verbose == 1:
        sep = ", "
        need_lines = "type id login".split()
    elif verbose == 2:
        sep = "\n"
        need_lines = ("type id login passwd_hash empty").split()
    else:
        raise ValueError("Verbose must be in interval [1, 2]")

    return get_full_string(lines, need_lines, sep, verbose)


def tag2str(tag: Tag, verbose: int = None):
    lines = dict()
    lines["type"] = ("{}", "Tag")
    lines["id"] = (" ID: {}", tag.id)
    lines["name"] = (" Name: {}", tag.name)
    lines["empty"] = (" {}", "")

    if verbose == 1:
        sep = ", "
        need_lines = "type id name".split()
    elif verbose == 2:
        sep = "\n"
        need_lines = ("type id name empty").split()
    else:
        raise ValueError("Verbose must be in interval [1, 2]")

    return get_full_string(lines, need_lines, sep, verbose)


def category2str(category: Category, verbose: int = None):
    lines = dict()
    lines["type"] = ("{}", "Category")
    lines["id"] = (" ID: {}", category.id)
    lines["name"] = (" Name: {}", category.name)
    lines["empty"] = (" {}", "")

    if verbose == 1:
        sep = ", "
        need_lines = "type id name".split()
    elif verbose == 2:
        sep = "\n"
        need_lines = ("type id name empty").split()
    else:
        raise ValueError("Verbose must be in interval [1, 2]")

    return get_full_string(lines, need_lines, sep, verbose)


def comment2str(comment: Comment, verbose: int = None):
    lines = dict()
    lines["type"] = ("{}", "Comment")
    lines["id"] = (" ID: {}", comment.id)
    lines["empty"] = (" {}", "")
    lines["content"] = (" Content: {}", comment.content)
    lines["creator_id"] = (" Creator: {}", comment.creator_id)
    lines["post_id"] = (" Post: {}", comment.post_id)

    if verbose == 1:
        sep = ", "
        need_lines = "type id content".split()
    elif verbose == 2:
        sep = "\n"
        need_lines = ("type id content creator_id post_id empty").split()
    else:
        raise ValueError("Verbose must be in interval [1, 2]")

    return get_full_string(lines, need_lines, sep, verbose)
