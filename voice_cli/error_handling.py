from functools import wraps


def error_handler_wrapper(f):
    """Decorator for presenting Exception occupied in function 'f'."""

    @wraps(f)
    def new_f(*args, **kwargs):
        try:
            f(*args, **kwargs)
        except Exception as e:
            raise e
            print("Error!")
            print(e)
            exit(1)

    return new_f
