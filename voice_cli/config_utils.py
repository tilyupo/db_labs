import os
import configparser
import logging

# ini-config keys
LOGGING_SECTION_INI_KEY = 'Logging'
FORMATING_SECTION_INI_KEY = 'Formating'
DATABASE_SECTION_INI_KEY = 'Database'
AUTHENTICATION_SECTION_INI_KEY = "Authentication"

LOGGING_FILE_INI_KEY = 'LoggingFile'
LOGGING_LEVEL_INI_KEY = 'Level'
INPUT_DATE_FORMAT_INI_KEY = 'InputDateFormat'
OUTPUT_DATE_FORMAT_INI_KEY = 'OutputDateFormat'
CONNECTION_STRING_INI_KEY = 'ConnectionString'
CURRENT_USER_INI_KEY = 'CurrentUser'
CURRENT_FAKE_INI_KEY = 'CurrentFake'
AUTO_SYNC_STATUS_INI_KEY = "AutoSynchronization"

LOG_LEVEL_MAPPING = {
    'DEBUG': logging.DEBUG,
    'INFO': logging.INFO,
    'WARN': logging.WARN,
    'WARNING': logging.WARNING,
    'ERROR': logging.ERROR,
}

# settings keys
CONNECTION_STRING_KEY = 'connection_string'
INPUT_DATE_FORMAT_KEY = 'input_date_format'
OUTPUT_DATE_FORMAT_KEY = 'output_date_format'
LOGGING_FILE_KEY = 'logging_file'
LOGGING_LEVEL_KEY = 'logging_level'
CURRENT_USER_KEY = 'current_user'
CURRENT_FAKE_KEY = 'current_fake'
AUTO_SYNC_STATUS_KEY = "auto_synchronization"


class SettingsProvider:
    """Contains settings for voice_cli."""

    def __init__(self, settings):
        self._settings = settings.copy()

    def set(self, key, value):
        """Set setting with passed key to value"""
        self._settings[key] = value

    def get(self, key):
        """Return setting with passed key"""
        return self._settings[key]

    @property
    def input_date_format(self) -> str:
        """Shortcut to get setting with INPUT_DATE_FORMAT_KEY key"""
        return self.get(INPUT_DATE_FORMAT_KEY)

    @property
    def output_date_format(self) -> str:
        """Shortcut to get setting with OUTPUT_DATE_FORMAT_KEY key"""
        return self.get(OUTPUT_DATE_FORMAT_KEY)

    @property
    def connection_string(self) -> str:
        """Shortcut to get setting with CONNECTION_STRING_KEY key"""
        return self.get(CONNECTION_STRING_KEY)

    @property
    def logging_file(self) -> str:
        """Shortcut to get setting with LOGGING_FILE_KEY key"""
        return self.get(LOGGING_FILE_KEY)

    @property
    def logging_level(self) -> int:
        """Shortcut to get setting with LOGGING_LEVEL_KEY key"""
        return self.get(LOGGING_LEVEL_KEY)

    @property
    def current_user(self) -> int:
        """Shortcut to get setting with CURRENT_USER_KEY key"""
        return self.get(CURRENT_USER_KEY)

    @property
    def current_fake(self) -> int:
        """Shortcur to get setting with CURRENT_USER_key key"""
        return self.get(CURRENT_FAKE_KEY)

    @property
    def auto_sync_flag(self) -> int:
        """Shortcut to get setting with AUTO_SYNC_STATUS_KEY key"""
        return self.get(AUTO_SYNC_STATUS_KEY)


def get_default_config_path():
    return os.path.join(os.path.expanduser('~'), '.voice', 'voice_config.ini')


def get_settings_from_config(config_path):
    config = configparser.ConfigParser()
    config.read(config_path)

    settings = {
        LOGGING_FILE_KEY: config[LOGGING_SECTION_INI_KEY][LOGGING_FILE_INI_KEY],
        LOGGING_LEVEL_KEY: get_log_level_by_str(config[LOGGING_SECTION_INI_KEY][LOGGING_LEVEL_INI_KEY]),
        INPUT_DATE_FORMAT_KEY: config[FORMATING_SECTION_INI_KEY][INPUT_DATE_FORMAT_INI_KEY],
        OUTPUT_DATE_FORMAT_KEY: config[FORMATING_SECTION_INI_KEY][OUTPUT_DATE_FORMAT_INI_KEY],
        CONNECTION_STRING_KEY: config[DATABASE_SECTION_INI_KEY][CONNECTION_STRING_INI_KEY],
        CURRENT_USER_KEY: config[AUTHENTICATION_SECTION_INI_KEY][CURRENT_USER_INI_KEY],
        CURRENT_FAKE_KEY: config[AUTHENTICATION_SECTION_INI_KEY][CURRENT_FAKE_INI_KEY],
        AUTO_SYNC_STATUS_KEY: config[AUTHENTICATION_SECTION_INI_KEY][AUTO_SYNC_STATUS_INI_KEY].lower() == "true",
    }

    return SettingsProvider(settings)


def change_current_user(config_path: str, new_user: str):
    config = configparser.ConfigParser()
    config.read(config_path)

    config[AUTHENTICATION_SECTION_INI_KEY][CURRENT_USER_INI_KEY] = new_user

    with open(config_path, 'w') as f:
        config.write(f)


def change_current_fake(config_path: str, new_fake: str):
    config = configparser.ConfigParser()
    config.read(config_path)

    config[AUTHENTICATION_SECTION_INI_KEY][CURRENT_FAKE_INI_KEY] = new_fake

    with open(config_path, 'w') as f:
        config.write(f)


def change_auto_sync(config_path: str, auto_sync_flag: bool):
    config = configparser.ConfigParser()
    config.read(config_path)

    config[AUTHENTICATION_SECTION_INI_KEY][AUTO_SYNC_STATUS_INI_KEY] = str(auto_sync_flag)

    with open(config_path, 'w') as f:
        config.write(f)


def get_log_level_by_str(level: str):
    if level.upper() in LOG_LEVEL_MAPPING:
        return LOG_LEVEL_MAPPING[level.upper()]
    else:
        raise ValueError("Invalid level string representation")
