import click
import re

from datetime import datetime, timedelta
from tzlocal import get_localzone


def validate_week_mask(ctx, param, value):
    """
        Validate that 'value' is a string representing week_mask
        in following format: exactly seven chars 0 or 1.
        Return represent representing by 'value'.
        If 'value' is incorrect week_mask, raise exception.

        Raises:
            click.BadParameter
    """
    if value is None:
        return None

    error = click.BadParameter("Invalid week mask. Mask must be in format: '0010011', but"
                               " {} recieved.".format(value))

    if len(value) != 7:
        raise error

    for letter in value:
        if letter not in "01":
            raise error

    if "1" not in value:
        raise error

    return value


def validate_deltatime(ctx, param, value):
    """
        Validate that 'value' is a string representing timedelta
        in following format: '<days> <hours>:<minutes>.
        Return represent representing by 'value'.
        If 'value' is incorrect timedelta, raise exception.

        Raises:
            click.BadParameter
    """
    if value is None:
        return None

    re_pattern = r"^(\d*) ([01]\d|2[0-3]):([0-5]\d)$"

    result = re.match(re_pattern, value)

    if result is None:
        raise click.BadParameter("Invalid deltatime format. Patern is '{}'".format(re_pattern))

    days = int(result.groups()[0])
    hours = int(result.groups()[1])
    minutes = int(result.groups()[2])

    result = timedelta(days=days, hours=hours, minutes=minutes)

    return result


def validate_datetime(ctx, param, value):
    """
        Validate that value is a string representing datetime
        in correct format. Return datetime representing by 'value'.
        If 'value' is incorrect datetime, raise exception.

        Raises:
            click.BadParameter
    """
    if value is None:
        return None

    date_format = ctx.obj.settings.input_date_format

    try:
        value = datetime.strptime(value, date_format)
        value = get_localzone().localize(value)

    except ValueError:
        msg = "This datetime must be inputed in format {}.".format(date_format)

        raise click.BadParameter(message=msg)

    return value


def validate_ids_list(ctx, param, value):
    """
        Validate that value is a string from digit and spaces.
        Return list of ints containing in 'value'.
        If 'value' contain something else, raise exception.

        Raises:
            click.BadParameter
    """
    if value is None:
        return None

    for item in value:
        if not item.isdigit():
            raise click.BadParameter("Ids must be integer, but '{}' recieved".format(item))

    return [int(item) for item in value]
