from voice import Voice
from functools import wraps
from voice_cli.exceptions import PermissionError
from voice.models import Fake, User


def pass_current_user_id(f):
    @wraps(f)
    def new_f(ctx, *args, **kwargs):
        voice = ctx.voice

        username = ctx.settings.current_user
        users = voice.user_service.select_by_logins([username])
        if len(users) == 0:
            user = User(login=username)
            user_id = voice.user_service.add(user)
            print("User '{}' does not exist.\n"
                  "So, it will be created with id {}.".format(
                      username,
                      user_id,
                  ))
        else:
            user_id = users[0].id

        fakename = ctx.settings.current_fake
        fakes = voice.fake_service.select_by_names(user_id=user_id, names=[fakename])
        if len(fakes) == 0:
            fake = Fake(nickname=fakename, user_id=user_id)
            fake_id = voice.fake_service.add(fake)
            print("Fake '{}' does not exist.\n"
                  "So, it will be created with id {}.".format(
                      fakename,
                      fake_id,
                  ))
        else:
            fake_id = fakes[0].id

        result = f(ctx, user_id, fake_id, *args, **kwargs)
        return result

    return new_f


def pass_current_user_id_factory(ctx_factory):
    def pass_current_user_id_2(f):
        @wraps(f)
        def new_f(*args, **kwargs):
            ctx = ctx_factory()

            voice = ctx.voice

            username = ctx.settings.current_user
            users = voice.user_service.select_by_logins([username])
            if len(users) == 0:
                user = User(login=username)
                user_id = voice.user_service.add(user)
                print("User '{}' does not exist.\n"
                      "So, it will be created with id {}.".format(
                          username,
                          user_id,
                      ))
            else:
                user_id = users[0].id

            fakename = ctx.settings.current_fake
            fakes = voice.fake_service.select_by_names(user_id=user_id, names=[fakename])
            if len(fakes) == 0:
                fake = Fake(nickname=fakename, user_id=user_id)
                fake_id = voice.fake_service.add(fake)
                print("Fake '{}' does not exist.\n"
                      "So, it will be created with id {}.".format(
                          fakename,
                          fake_id,
                      ))
            else:
                fake_id = fakes[0].id

            result = f(user_id, fake_id, *args, **kwargs)
            return result

        return new_f
    return pass_current_user_id_2


# region task_permission
def check_task_read_permission(voice: Voice, user_id: int, task_id: int):
    """
    If user has no permission for watch the task, raise exception.

    Args:
        voice: Voice object, manager of entities
        user_id: id of user
        task_id: id of task

    Raises:
        PermissionError
    """
    user = voice.get_object("user", user_id)
    task = voice.get_object("task", task_id)

    if (user is not task.creator and user not in task.moderators and user not in task.doers
            and user not in task.watchers):
        raise PermissionError("User {} has no permission for "
                              "watching the Task {}.".format(
                                  user.name,
                                  task.id,
                              ))


def check_task_change_status_permission(voice: Voice, user_id: int, task_id: int):
    """
    If user has no permission for marking/unmarking this task as done,
    raise exception.

    Args:
        voice: Voice object, manager of entities
        user_id: id of user
        task_id: id of task

    Raises:
        PermissionError
    """
    user = voice.get_object("user", user_id)
    task = voice.get_object("task", task_id)

    if (user is not task.creator and user not in task.moderators and user not in task.doers):
        raise PermissionError("User {} have not privilages for"
                              " changing status of Task {}.".format(
                                  user.name,
                                  task.id,
                              ))


def check_task_edit_permission(voice: Voice, user_id: int, task_id: int):
    """
    If user has no permission for editing task, raise exception.

    Args:
        voice: Voice object, manager of entities
        user_id: id of user
        task_id: id of task

    Raises:
        PermissionError
    """
    user = voice.get_object("user", user_id)
    task = voice.get_object("task", task_id)

    if (user is not task.creator and user not in task.moderators):
        raise PermissionError("User {} has no permission for editing Task {}.".format(
            user.name,
            task.id,
        ))


def check_task_remove_permission(voice: Voice, user_id: int, task_id: int):
    """
    If user has no permission for removing task, raise exception.

    Args:
        voice: Voice object, manager of entities
        user_id: id of user
        task_id: id of task

    Raises:
        PermissionError
    """
    user = voice.get_object("user", user_id)
    task = voice.get_object("task", task_id)

    if user is not task.creator:
        raise PermissionError("User {} has no permission for removing Task {}.".format(
            user.name,
            task.id,
        ))


def check_task_make_user_relation_permission(voice: Voice, user_id: int, task_id: int):
    """
    If user has no permission for editing task, raise exception.

    Args:
        voice: Voice object, manager of entities
        user_id: id of user
        task_id: id of task

    Raises:
        PermissionError
    """
    user = voice.get_object("user", user_id)
    task = voice.get_object("task", task_id)

    if user is not task.creator:
        raise PermissionError("User {} has no permission for"
                              " making relation for Task {}.".format(
                                  user.name,
                                  task.id,
                              ))


# endregion


# region supertask_permission
def check_supertask_read_permission(voice: Voice, user_id: int, supertask_id: int):
    """
    If user has no permission for watch the supertask, raise exception.

    Args:
        voice: Voice object, manager of entities
        user_id: id of user
        supertask_id: id of supertask

    Raises:
        PermissionError
    """
    user = voice.get_object("user", user_id)
    supertask = voice.get_object("supertask", supertask_id)

    if (user is not supertask.creator and user not in supertask.moderators and user not in supertask.doers
            and user not in supertask.watchers):
        raise PermissionError("User {} has no permission for "
                              "watching the supertask {}.".format(
                                  user.name,
                                  supertask.id,
                              ))


def check_supertask_edit_permission(voice: Voice, user_id: int, supertask_id: int):
    """
    If user has no permission for editing supertask, raise exception.

    Args:
        voice: Voice object, manager of entities
        user_id: id of user
        supertask_id: id of supertask

    Raises:
        PermissionError
    """
    user = voice.get_object("user", user_id)
    supertask = voice.get_object("supertask", supertask_id)

    if (user is not supertask.creator and user not in supertask.moderators):
        raise PermissionError("User {} has no permission for editing supertask {}.".format(
            user.name,
            supertask.id,
        ))


def check_supertask_remove_permission(voice: Voice, user_id: int, supertask_id: int):
    """
    If user has no permission for removing supertask, raise exception.

    Args:
        voice: Voice object, manager of entities
        user_id: id of user
        supertask_id: id of supertask

    Raises:
        PermissionError
    """
    user = voice.get_object("user", user_id)
    supertask = voice.get_object("supertask", supertask_id)

    if user is not supertask.creator:
        raise PermissionError("User {} has no permission for removing supertask {}.".format(
            user.name,
            supertask.id,
        ))


def check_supertask_make_user_relation_permission(voice: Voice, user_id: int, supertask_id: int):
    """
    If user has no permission for editing supertask, raise exception.

    Args:
        voice: Voice object, manager of entities
        user_id: id of user
        supertask_id: id of supertask

    Raises:
        PermissionError
    """
    user = voice.get_object("user", user_id)
    supertask = voice.get_object("supertask", supertask_id)

    if user is not supertask.creator:
        raise PermissionError("User {} has no permission for"
                              " making relation for supertask {}.".format(
                                  user.name,
                                  supertask.id,
                              ))


# endregion


def check_user_trusting_permission(voice: Voice, user_id: int, submissive_id: int):
    """
    If submissive user doesn't trust user_id, raise exception.
    Also we expect that user trust himself.

    Args:
        voice: Voice object, manager of entities
        user_id: id of loged user
        submissive_id: id of submissive user

    Raises:
        PermissionError
    """
    current_user = voice.get_object("user", user_id)
    submissive_user = voice.get_object("user", submissive_id)

    if (current_user != submissive_user and submissive_user not in current_user.trustings):
        raise PermissionError("User {} does not trust the User {}."
                              "So User {} cannot add to or remove from *.doers"
                              " User {}, show his supertasks.".format(
                                  submissive_id,
                                  user_id,
                                  user_id,
                                  submissive_id,
                              ))
